# Origin (Console)

A console application for Origin (and other) WordPress development projects.

**This application is in early alpha. It will change. It will break. It will drive you crazy.**

## Requirements

* PHP 5.4 or later
* WordPress 3.5.2 or later
* [WP-CLI](http://wp-cli.org/)

This project is designed to be cross-platform, however windows support is limited.

If you need further help getting started with this or any other tools we use, please [see this repository](https://bitbucket.org/ltconsulting/getting-started).

## Installation

First download [origin.phar](https://bitbucket.org/origin-wp/console/raw/master/dist/origin.phar). You can do this manually or using `wget` or `curl`. For example:

````
curl -O https://bitbucket.org/origin-wp/console/raw/master/dist/origin.phar
````

Then, check if it works:

````
php origin.phar
````

Next, move the file into your PATH. For example:

````
chmod +x origin.phar
sudo mv origin.phar /usr/local/bin/origin
````

Now try running `origin` to check if it works.

### Installation on Windows

If you are on a PC, you may need to add a batch file to allow the file to be run by the Command Line.

First move the .phar file to a location where it can reside permanently, perhaps in the `C:\` directory. Make a new directory called 'origin' and put the file there. Next, create a new .bat file in that location using your text editor. Make sure this file doesn't have another hidden extension. 

Paste the following into the file and save it:

```
@ECHO OFF
php "c:/origin/origin.phar" %*
```

Now you can add this to your PATH variable by opening the control panel, finding environment variables, and saving the path to the .bat file.

Restart your command prompt and try running `origin` to check if it works.]

If that didn't work, please visit the troubleshooting section of the [getting started repository](https://bitbucket.org/ltconsulting/getting-started). 

## Configuration

### WP CLI

Please ensure your system is configured to work correctly with [WP CLI](http://wp-cli.org/config/).

### Environment

You may need to include an Origin configuration file if your system doesn't match our defaults. This should be stored in `~/.origin/config.json`. You can use a custom directory by settings the environment variable `ORIGIN_CONFIG_PATH` to the config path.

Here is an example configuration file:

````
{
    "database": {
        "host": "localhost",
        "user": "root",
        "pass": "root"
    },
    "server": {
        "port": "80"
    }
}
````

Please note that the script is configured to assume your hostname is "localhost" and your user and password are "root". If this is not the case, you may need to reconfigure your environment to match the script.

## Updates

You're responsible for ensuring you are using the latest version of origin.phar.

You can update manually using `origin self-update`, however it's recommended that you add the following cron entry for automatic updates.

````
0 0 * * * origin self-update
````

## Commands

You can see all available commands by running the following:

````
origin list
````

## Builds

If you're planning to contribute to this project, you will need to clone down this repository and install dependencies with composer:

````
composer install
````

You will need to install [Box2](https://github.com/box-project/box2) first. Then run the following to build a new phar:

````
box build
````