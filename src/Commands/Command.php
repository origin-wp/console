<?php

namespace Origin\Console\Commands;

use Symfony\Component\Process\Process;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command as SymfonyCommand;

abstract class Command extends SymfonyCommand
{
    /**
     * The input interface implementation.
     *
     * @var Symfony\Component\Console\Input\InputInterface
     */
    protected $input;

    /**
     * The output interface implementation.
     *
     * @var Symfony\Component\Console\Output\OutputInterface
     */
    protected $output;

    /**
     * Set the input/output to a class property.
     *
     * @param InputInterface  $i
     * @param OutputInterface $o
     */
    protected function setIO(InputInterface $i, OutputInterface $o)
    {
        $this->input = $i;
        $this->output = $o;
    }

    /**
     * Write a comment to the console.
     *
     * @param string $comment
     */
    protected function comment($comment)
    {
        $this->output->writeln("<comment>$comment</comment>");
    }

    /**
     * Write an error to the console.
     *
     * @param string $comment
     */
    protected function error($error)
    {
        $this->output->writeln("<error>$error</error>");
    }

    /**
     * Write an info statement to the console.
     *
     * @param string $comment
     */
    protected function info($info)
    {
        $this->output->writeln("<info>$info</info>");
    }

    /**
     * Write a question to the console.
     *
     * @param string $comment
     */
    protected function question($question)
    {
        $this->output->writeln("<question>$question</question>");
    }

    /**
     * Write a message to the console.
     *
     * @param string $$message
     */
    protected function write($message)
    {
        $this->output->writeln("$message");
    }

    /**
     * Run a process and display the output.
     *
     * @param $command
     * @return int
     */
    protected function process($command)
    {
        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            $error = trim($process->getErrorOutput());
            $this->error($error);

            return 1;
        }

        $output = trim($process->getOutput());
        $this->write($output);

        return $process->getOutput();
    }

    /**
     * Get 'config' directory for config file
     *
     * @return string
     */
    protected function config()
    {
        $dir = false;

        if (getenv('ORIGIN_CONFIG_PATH')) {
            $path = getenv('ORIGIN_CONFIG_PATH');
        } else {
            $path = getenv('HOME') . '/.origin/config.json';
        }

        if(is_readable($path)) {
            return $path;
        }

        return false;
    }

    /**
     * Are we on a windows environment?
     *
     * @return boolean
     */
    protected function isWindows()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return true;
        }

        return false;
    }

    /**
     * Copy a string of data to the clipboard
     *
     * @param mixed $string
     * @return mixed
     */
    protected function copyToClipboard($string)
    {
        if ($this->isWindows()) {
            return $this->process("echo \"$string\" | clip");
        }

        return $this->process("echo \"$string\" | pbcopy");
    }

    /**
     * Append --$key="" to the proxied command if
     * exists in our command
     *
     * @param InputInterface $input
     * @param $key
     * @return string
     */
    protected function addOptionIfExists(InputInterface $input, $key)
    {
        $option = $input->getOption($key);
        
        return !empty($option) ? " --{$key}=\"{$option}\"" : '';
    }
}
