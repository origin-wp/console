<?php

namespace Origin\Console\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeWidgetCommand extends Command
{
    /**
     * Setup configuration for the update command.
     */
    protected function configure()
    {
        $this->setName('make:widget')
            ->addArgument('name', InputArgument::REQUIRED, 'The unique ID of the Widget.')
            ->addOption('fields', 'f', InputOption::VALUE_OPTIONAL, 'A comma seperated list of fields.')
            ->setDescription('Create a new Origin powered Widget.');
    }

    /**
     * Logic for the make:widget command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setIO($input, $output);
        
        $this->write('Creating Widget..');
        
        $this->process('wp origin widget ' . $input->getArgument('name').$this->addOptionIfExists($input, 'fields'));
    }
}
