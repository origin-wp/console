<?php

namespace Origin\Console\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeMetaBoxCommand extends Command
{
    /**
     * Setup configuration for the Metabox command.
     * 
     * @return void
     */
    protected function configure()
    {
        $this->setName('make:metabox')
            ->addArgument('name', InputArgument::REQUIRED, 'The unique name of the Meta Box.')
            ->addOption('fields', 'f', InputOption::VALUE_OPTIONAL, 'A comma seperated list of fields.')
            ->setDescription('Create a new Origin powered Meta Box.');
    }

    /**
     * Logic for the make:metabox command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setIO($input, $output);

        $this->write('Creating Meta box..');

        $this->process('wp origin metabox ' . $input->getArgument('name').$this->addOptionIfExists($input, 'fields'));
    }
}
