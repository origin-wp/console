<?php

namespace Origin\Console\Commands;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MakeShortcodeCommand extends Command
{
    /**
     * Setup configuration for the Shortcode command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('make:shortcode')
            ->addArgument('name', InputArgument::REQUIRED, 'The unique tag of the Shortcode.')
            ->addOption('fields', 'f', InputOption::VALUE_OPTIONAL, 'A comma seperated list of fields.')
            ->setDescription('Create a new Origin powered Shortcode.');
    }

    /**
     * Logic for the make:metabox command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setIO($input, $output);

        $this->write('Creating Shortcode..');

        $this->process('wp origin shortcode ' . $input->getArgument('name').$this->addOptionIfExists($input, 'fields'));
    }
}
