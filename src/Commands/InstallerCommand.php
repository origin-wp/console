<?php

namespace Origin\Console\Commands;

use Origin\Console\Password;
use Symfony\Component\Process\Process;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class InstallerCommand extends Command
{
    /**
     * WordPress plugins to install.
     *
     * @var array
     */
    protected $plugins = [
        'wp-org' => [
            'antispam-bee' => true,
            'wordpress-seo' => true,
            'customizer-remove-all-parts' => true,
            'disable-emojis' => true,
            'bootstrap-3-shortcodes' => true,
            'google-analytics-for-wordpress' => false,
            'tiny-compress-images' => true,
            'remove-query-strings-from-static-resources' => true,
            'worker' => false
        ],
        'git' => [
            'indigotree' => [
                'repository' => 'git@bitbucket.org:ltconsulting/indigotree.git',
                'activate' => true,
            ],
            'origin-developer' => [
                'repository' => 'git@bitbucket.org:ltconsulting/developer.git',
                'activate' => true,
            ],
        ],
        'zip' => [
            
        ]
    ];

    /**
     * Setup configuration for the install command.
     */
    protected function configure()
    {
        $this->setName('wp:install')->setDescription('Install WordPress from a pre-defined boilerplate.');
    }

    /**
     * Logic for the install command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $h = $this->getHelper('question');

        // set the input/output properties
        $this->setIO($input, $output);

        // get configuration data
        $config = $this->config() ? json_decode(file_get_contents($this->config()), true): [];

        // parse the current directory name
        $cwd = basename(getcwd());

        // set the database details
        $db_name = $cwd;

        $db_host = isset($config['database']['host']) ? trim($config['database']['host']) : 'localhost';
        $db_user = isset($config['database']['user']) ? trim($config['database']['user']) : 'root';
        $db_pass = isset($config['database']['pass']) ? trim($config['database']['pass']) : 'root';

        $db_host = !empty($db_host) ? ' --dbhost="' . $db_host . '"' : '';
        $db_user = !empty($db_user) ? ' --dbuser="' . $db_user . '"' : '';
        $db_pass = !empty($db_pass) ? ' --dbpass="' . $db_pass . '"' : '';

        $port = isset($config['server']['port']) ? trim($config['server']['port']) : '80';

        $root_url = $port == 80 ? 'localhost' : 'localhost:' . $port;

        // ask for the site name
        $site_name = $h->ask($input, $output, new Question('Site Name: ', $cwd));

        // check if they want to go ahead with this (simple precaution)
        if (!$h->ask($input, $output, new ConfirmationQuestion('Run Install? (y/n)'))) {
            $this->error('Darn. Installation cancelled.');

            return;
        }

        // download wordpress core files
        $output->write('Getting WordPress..');

        (new Process('wp core download'))->run(function ($type, $buffer) {
            $method = Process::ERR === $type ? 'error' : 'write';
            $this->{$method}($buffer);
        });

        // setup new wp-config file
        $output->write('Setting up wp-config.php..');
        $this->setupConfig($db_host, $db_user, $db_pass, $db_name);

        // create a new database
        $output->write('Creating mysql database..');
        $this->process('wp db create');

        // create a random password
        $password = new Password();

        // copy the password to the clipboard
        $this->copyToClipboard($password);

        // install wordpress :)
        $output->write('Installing wordpress..');
        $this->process("wp core install --url=\"http://{$root_url}/{$cwd}\" --title=\"{$site_name}\" --admin_user=\"indigotree\" --admin_password=\"{$password}\" --admin_email=\"website@indigotree.co.uk\"");

        // modify the default user
        $this->process('wp user update 1 --user_url="https://indigotree.co.uk" --first_name="Indigo Tree Digital" --display_name="Indigo Tree Digital"');

        // rename the first category to news
        $this->process('wp term update category 1 --name="News" --slug="news"');

        // hide the site from search engines
        $this->process('wp option update blog_public 0');

        // show only 6 blog posts per page
        $this->process('wp option update posts_per_page 6');

        // Set language to en-GB
        $this->process('wp core language install --activate en_GB');

        // set a local timezone
        $this->process('wp option update timezone_string \'Europe/London\'');

        // remove blog description
        $this->process('wp option update blogdescription \'\'');

        // allow threaded comemnts
        $this->process('wp option update thread_comments 1');

        // allow threaded comments to 2 levels deep
        $this->process('wp option update thread_comments_depth 2');

        // set uploads to not use year/month folders
        $this->process('wp option update uploads_use_yearmonth_folders 0');

        // delete the default sample page
        $id = $this->process('wp post list --post_type=page --posts_per_page=1 --post_status=publish --pagename="sample-page" --field=ID --format=ids');
        $this->process("wp post delete {$id}");

        // create a home page
        $this->process('wp post create --post_type=page --post_title=Home --post_status=publish --post_author=1');

        // display a page as the front page of the site
        $this->process('wp option update show_on_front \'page\'');

        // set our new homepage to be the frontpage
        $id = $this->process('wp post list --post_type=page --post_status=publish --posts_per_page=1 --pagename=home --field=ID --format=ids');
        $this->process("wp option update page_on_front {$id}");

        // set the rewrite rules
        if ($this->isWindows()) {
            $this->process('start cmd /c wp rewrite structure /%postname%/ --hard');
            $this->process('start cmd /c wp rewrite flush --hard');
        } else {
            $this->process('wp rewrite structure \'/%postname%/\' --hard');
            $this->process('wp rewrite flush --hard');
        }

        $this->plugins();

        $this->themes();

        $home = $this->process('wp option get home');

        if ($this->isWindows()) {
            $this->process("start $home");
        } else {
            $this->process("open $home");
        }

        $this->process("composer install -d ./wp-content/themes/origin-theme");

        // ------

        $this->write('=================================================================');
        $this->write('Installation is complete. Your username/password is listed below.');
        $this->write(' ');
        $this->write('User: ltconsulting');
        $this->write("Pass: {$password}");
    }

    /**
     * Add/Remove WordPress plugins.
     */
    protected function plugins()
    {
        // delete default plugins
        $this->process('wp plugin delete akismet');
        $this->process('wp plugin delete hello');

        // install wordpress plugins
        foreach ($this->plugins['wp-org'] as $plugin => $to_activate) {
            $activate = $to_activate ? ' --activate' : '';
            $this->process("wp plugin install {$plugin}{$activate}");
        }

        // install wordpress plugins
        foreach ($this->plugins['zip'] as $plugin => $to_activate) {
            $activate = $to_activate ? ' --activate' : '';
            $this->process("wp plugin install {$plugin}{$activate}");
        }

        // install wordpress plugins from git
        foreach ($this->plugins['git'] as $dir => $plugin) {
            $this->process("git clone {$plugin['repository']} ./wp-content/plugins/{$dir}");
            $this->process("wp plugin activate {$dir}");
        }
    }

    /**
     * Add/Remove WordPress themes.
     */
    protected function themes()
    {
        $this->process('git clone git@bitbucket.org:ltconsulting/theme.git ./wp-content/themes/origin-theme');
        $this->process('wp theme activate origin-theme');

        if ($this->isWindows()) {
            $this->process('rmdir /s /q ./wp-content/themes/origin-theme/.git');
        } else {
            $this->process('rm -rf ./wp-content/themes/origin-theme/.git');
        }
    }

    /**
     * Setup wp-config.php file
     *
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $name
     * @return mixed
     */
    protected function setupConfig($host, $user, $pass, $name)
    {
        $prefix = $append = '';

        if ($this->isWindows()) {

            $prefix = "(echo define^^^('WP_DEBUG', true^^^);";
            $prefix .= " & echo define^^^('DISALLOW_FILE_EDIT', true^^^);";
            $prefix .= " & echo define^^^('AUTOSAVE_INTERVAL', 600^^^);";
            $prefix .= " & echo define^^^('WP_MEMORY_LIMIT', '256M'^^^);";
            $prefix .= " & echo @ini_set^^^('upload_max_filesize', '256M'^^^);";
            $prefix .= " & echo @ini_set^^^('max_post_size', '256M'^^^);";
            $prefix .= " & echo @ini_set^^^('max_input_vars', 5000^^^);";
            $prefix .= " & echo @set_time_limit^^^(300^^^);) | ";

        } else {

            $append = " <<PHP".PHP_EOL.PHP_EOL;
            $append .= "define('WP_DEBUG', true);".PHP_EOL;
            $append .= "define('DISALLOW_FILE_EDIT', true);".PHP_EOL;
            $append .= "define('AUTOSAVE_INTERVAL', 600);".PHP_EOL.PHP_EOL;
            $append .= "define('WP_MEMORY_LIMIT', '256M');".PHP_EOL;
            $append .= "define('WP_MAX_MEMORY_LIMIT', '256M');".PHP_EOL.PHP_EOL;
            $append .= "@ini_set('upload_max_filesize', '256M');".PHP_EOL;
            $append .= "@ini_set('max_post_size', '256M');".PHP_EOL;
            $append .= "@ini_set('max_input_vars', 5000);".PHP_EOL.PHP_EOL;
            $append .= "@set_time_limit(300);";

        }

        return (new Process($prefix."wp core config --dbname=\"{$name}\"{$user}{$pass}{$host} --extra-php".$append))->run(function ($type, $buffer) {
            $method = Process::ERR === $type ? 'error' : 'write';
            $this->{$method}($buffer);
        });
    }
}
