<?php

namespace Origin\Console\Commands;

use Herrera\Phar\Update\Manager;
use Herrera\Phar\Update\Manifest;
use Herrera\Json\Exception\FileException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateCommand extends Command
{
    /**
     * The full path to the update manifest file.
     * 
     * @var string
     */
    protected $manifest = 'https://bitbucket.org/origin-wp/console/raw/master/manifest.json';

    /**
     * Setup configuration for the update command.
     */
    protected function configure()
    {
        $this->setName('self-update')->setDescription('Updates origin.phar to the latest version');
    }

    /**
     * Logic for the update command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Searching for updates...');

        try {
            $manager = new Manager(Manifest::loadFile($this->manifest));
        } catch (FileException $e) {
            $output->writeln('<error>Unable to read update manifest</error>');
            $output->writeln($e->getMessage());

            return 1;
        }

        if ($manager->update($this->getApplication()->getVersion(), true)) {
            $output->writeln('<info>Updated to latest version</info>');
        } else {
            $output->writeln('<comment>Already at the latest version</comment>');
        }
    }
}
