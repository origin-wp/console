<?php

namespace Origin\Console;

class Password
{
    protected $alphabet = 'abcdefghijklmnopqrstuvwxyz';

    protected $numbers = '1234567890';

    protected $symbols = '_-!@£$%&*+/?{}()[]';

    protected $length = 12;

    protected $characters = '';

    /**
     * Generate a random password.
     *
     * @return string
     */
    public function generate()
    {
        $this->characters = $this->alphabet.mb_strtoupper($this->alphabet).$this->numbers.$this->symbols;
        $password = [];

        for ($i = 0; $i < $this->length; ++$i) {
            $password[] = $this->characters[rand(0, strlen($this->characters) - 1)];
        }

        return implode($password);
    }

    /**
     * Generate a random password.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->generate();
    }
}
